import socket
import threading
import os

import wget

port = 1911  # port of the server
socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # socket
socket.bind(('', port))
socket.listen(1)

lock = threading.Lock()

user = "root"  # username
password = "admin"  # password


class daemon(threading.Thread):
    def __init__(self, array):
        threading.Thread.__init__(self)
        self.download_directory = None
        self.socket = array[0]  # socket
        self.address = array[1]  # address

    def recv_input(self):  # accumulate character and return a string
        input, data = "", ""
        while data != os.linesep:  # end of input with "ENTER"
            data = self.socket.recv(1024).decode()  # get next character
            input += data  # add it to the string
        return input[:-2]  # return the string without os.linesep character

    def run(self):  # main function
        self.socket.send("\r\nBenvenuto su Telnet Server".encode())  # send greetings
        input_user, input_password = "", ""  # initialize variables
        while user != input_user and password != input_password:  # authentication loop
            self.socket.send("\r\nInserire il nome utente: ".encode())
            input_user = self.recv_input()  # get username
            self.socket.send("Inserire la password: ".encode())
            input_password = self.recv_input()  # get password
        self.socket.send("Login effettuato con successo, opzioni disponibili.\r\n\r\n".encode())

        while True:  # main loop
            self.socket.send("\r\n\r\nOpzioni Disponibili\r\n\r\n"  # send choice
                             "1. Restituisce la Lista delle Directory\r\n"
                             "2. Crea una nuova directory nella directory corrente\r\n"
                             "3. Restituisce la lista dei File presenti in una Directory specifica\r\n"
                             "4. Download di uno dei file della Directory specifica\r\n"
                             "5. Esci\r\n".encode())
            choice = self.socket.recv(1024).decode()  # get choice
            if choice[0] == '1':  # list current dir
                result = '\r\n' + str(os.listdir()) + '\r\n'
                self.socket.send(result.encode())
            elif choice[0] == '2':  # create a new directory
                self.socket.send("\r\nDirectory: ".encode())
                directory = self.recv_input()  # get the name
                try:
                    os.mkdir(directory)  # make the dir
                    for item in os.listdir():
                        result = '\r\n' + str(item)  # list current dir (vertical)
                        self.socket.send(result.encode())
                except FileExistsError:  # check if directory already exist
                    self.socket.send("\r\nEsiste già una directory con il nome inserito".encode())
            elif choice[0] == '3':  # list dir of a specific directory
                self.socket.send("\r\nDirectory: ".encode())
                directory = self.recv_input()  # get the directory name
                if os.path.isdir(directory):  # check if directory exist
                    result = ""
                    for file in os.listdir(directory):  # list directory
                        if os.path.isfile(os.path.abspath(os.path.join(directory, file))):  # check if file is a file
                            result += '\r\n' + str(file) + '\r\n'  # accumulating all the files
                    if result == "":  # no file found
                        self.socket.send("\r\nNo file found\r\n".encode())
                    else:  # print files
                        self.download_directory = directory  # save location for choice 4
                        self.socket.send(result.encode())
                else:  # check if exist
                    self.socket.send("\r\nLa directory inserita non esiste\r\n".encode())
            elif choice[0] == '4':  # download a file in choice 3 directory
                if self.download_directory is not None:  # check if choice 3 was done before
                    self.socket.send("\r\nFile: ".encode())
                    file = self.recv_input()  # get the file name
                    if os.path.isfile(os.path.abspath(os.path.join(self.download_directory, file))):  # check if is a file
                        path = os.path.join(self.download_directory, file).replace("\\", "/")  # make the path
                        # url = f"http://localhost:{port}/{path}"  # make the url
                        # wget.download(url)  # download with wget
                        with open(path, "rb") as target:  # open the file
                            for data in target:
                                self.socket.send(data)  # send all the data
                    else:  # no file found
                        self.socket.send("\r\nIl file non esiste\r\n".encode())
                else:  # no dir found
                    self.socket.send("\r\nDevi usare prima l'opzione 3\r\n".encode())
            elif choice[0] == '5':  # close the connection
                break  # exit the loop
            else:  # invalid choice
                self.socket.send("\r\n\r\nOpzione non trovata\r\n\r\n".encode())
        self.socket.close()  # close the socket


while True:
    daemon(socket.accept()).start()
