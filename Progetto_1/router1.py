import select
import socket

socket_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # socket for the server
socket_server.bind(("127.0.0.1", 81))

socket_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # socket for the clients
socket_client.bind(("127.0.0.1", 83))
socket_client.listen(5)  # connections attempt
server = ("127.0.0.1", 80)  # server ip and port
BUF_SIZE = 1024  # set buffer size
checkTimeout = 1  # set timeout value (sec) for select.select
router_ip_s = "195.1.10.1"
router_ip_c = "92.10.10.1"
router_mac_s = "55:04:0A:EF:10:AB"
router_mac_c = "55:04:0A:EF:11:CF"
server_ip = "195.1.10.10"
server_mac = "52:AB:0A:DF:10:DC"

ONLINE = "Online"  # utility variable
OFFLINE = "Offline"
clients = []  # array of all clients socket
arp_socket = {}  # client ip -> client socket
arp_mac = {}  # client ip -> client mac


# compose and encode a packet given source IP, destination IP and MAC, message. The type is used to determine the MAC address
def compose_packet(source_ip, destination_ip, destination_mac, message, dest_type = "SERVER"):
    if dest_type == "CLIENT":  # the packet will go to a client
        IP_header = source_ip + "|" + destination_ip
        ethernet_header = router_mac_c + "|" + destination_mac
    else:  # the packet will go to the server
        IP_header = source_ip + "|" + destination_ip
        ethernet_header = router_mac_s + "|" + destination_mac
    packet = ethernet_header + "|" + IP_header + "|" + message
    return str.encode(packet)


# decode and construct dictionary with the information of array packet
def decompose_packet(message: bytes):
    tmp = message.decode("utf-8").split("|")
    if len(tmp) == 5:
        packet = {"source_mac": tmp[0],
                  "destination_mac": tmp[1],
                  "source_ip": tmp[2],
                  "destination_ip": tmp[3],
                  "message": tmp[4]}
        return packet
    else:
        return None


# parse a message and send a response to the proper destination
def parse_packet(message):
    if message['message'] == OFFLINE:
        print(f"Client {message['source_ip']} is offline")
        socket_server.send(compose_packet(message["source_ip"], server_ip, server_mac, message["message"], "SERVER"))
    elif message["source_mac"] == server_mac:  # server -> client
        d_socket = arp_socket[message["destination_ip"]]  # get the client socket
        print(f"New message from Server, forwarding to client {message['destination_ip']}")
        d_socket.send(compose_packet(message["source_ip"], message["destination_ip"], message["destination_mac"], message["message"], "CLIENT"))
    else:  # client -> server
        print(f"New message from {message['source_ip']}, to {message['destination_ip']}, forwarding to Server")
        socket_server.send(compose_packet(message["source_ip"], message["destination_ip"], message["destination_mac"], message["message"], "SERVER"))


# add a client info
def add_client_info(ip, mac, socket):
    arp_socket[ip] = socket  # link the arp tables
    arp_mac[ip] = mac
    for client in clients:  # check if the client already exist
        if client == socket:
            return
    clients.append(socket)  # add the client socket


# add a client
def add_new_client(socket):
    packet = decompose_packet(socket.recv(BUF_SIZE))
    if packet["message"] == "ACK":  # get the ack (IP, MAC and socket)
        add_client_info(packet["source_ip"], packet["source_mac"], socket)  # add the client
        print(f"Client {packet['source_ip']} is online")
        socket_server.send(compose_packet(packet["source_ip"], server_ip, server_mac, ONLINE))  # communicate to the server that the client is online
        socket.send(compose_packet(packet["source_ip"], packet["source_ip"], packet["source_mac"], "ACK", "CLIENT"))  # send back ACK
        print(f"Identification done.\nWaiting for new message...")
    else:  # packet not as expected
        print("Client not identified, close socket")
        socket.close()  # closing the socket


# check for new client
def check_for_new_connection():
    r_socket, _, _ = select.select([socket_client], [], [], checkTimeout)  # check for new client
    if not r_socket:  # no connections requests founds
        return None
    else:
        for i_socket in r_socket:  # requests founds
            socket, address = socket_client.accept()
            add_new_client(socket)  # add the new client


# check for new message
def check_for_message(client):
    r_socket, _, _ = select.select([client], [], [], checkTimeout)  # check for new message
    if not r_socket:  # no message found
        return None
    else:
        messages = []
        for i_socket in r_socket:  # messages found
            packet = i_socket.recv(1024)  # receive the packet
            if packet == b'':  # check if it has content
                continue
            else:
                messages.append(decompose_packet(packet))  # append the message to the list
        if messages:  # list non empty
            return messages  # return the messages
        else:  # no message found
            return None


print("Connecting to server...")
socket_server.connect(server)  # connect to the server
print("Connected, sending ACK")
socket_server.send(compose_packet(router_ip_s, server_ip, server_mac, "ACK"))  # identificate

print("Waiting for new connections...")
while True:
    check_for_new_connection()  # check for new client
    packets = [check_for_message(socket_server)]  # check for new message from server
    for client in clients:  # check for new message from every client
        packets.append(check_for_message(client))
    if packets is None:  # no new message
        continue
    else:
        for packet in packets:  # message found
            if packet is None:  # some may be empty
                continue
            else:  # parse all messages
                parse_packet(packet.pop())
