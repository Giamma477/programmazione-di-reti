server_ip = "195.1.10.10"
server_mac = "52:AB:0A:DF:10:DC"

router1_ip_s = "195.1.10.1"
router1_ip_c = "92.10.10.1"
router1_mac_s = "55:04:0A:EF:10:AB"
router1_mac_c = "55:04:0A:EF:11:CF"

router2_ip_s = "195.1.10.2"
router2_ip_c = "1.5.10.1"
router2_mac_s = "32:03:0A:DA:11:DC"
router2_mac_c = "32:03:0A:CF:10:DB"

router_table = {
    "router1": {"ip": "195.1.10.1", "mac": "55:04:0A:EF:10:AB"},
    "router2": {"ip": "195.1.10.2", "mac": "32:03:0A:CF:10:DB"},
}

client_table = {
    "client1": {"ip": "92.10.10.15", "mac": "32:04:0A:EF:19:CF"},
    "client2": {"ip": "92.10.10.20", "mac": "10:AF:CB:EF:19:CF"},
    "client3": {"ip": "92.10.10.25", "mac": "AF:04:67:EF:19:DA"},
    "client4": {"ip": "1.5.10.15", "mac": "42:A3:1B:DA:12:AC"},
    "client5": {"ip": "1.5.10.20", "mac": "42:A3:5B:DA:13:EF"},
    "client6": {"ip": "1.5.10.30", "mac": "44:BF:5B:DA:11:AC"}
}

client1_ip = "92.10.10.15"
client1_mac = "32:04:0A:EF:19:CF"
client2_ip = "92.10.10.20"
client2_mac = "10:AF:CB:EF:19:CF"
client3_ip = "92.10.10.25"
client3_mac = "AF:04:67:EF:19:DA"

client4_ip = "1.5.10.15"
client4_mac = "42:A3:1B:DA:12:AC"
client5_ip = "1.5.10.20"
client5_mac = "42:A3:5B:DA:13:EF"
client6_ip = "1.5.10.30"
client6_mac = "44:BF:5B:DA:11:AC"
