import select
import socket

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # server main socket
server.bind(("", 80))
server.listen(4)
BUF_SIZE = 1024  # set buffer size
checkTimeout = 1  # set timeout value (sec) for select.select
server_ip = "195.1.10.10"  # server IP
server_mac = "52:AB:0A:DF:10:DC"  # server MAC

ONLINE = "Online"  # utility variable
OFFLINE = "Offline"
client_table = {}  # dictionary of all client logged at least once. Client -> ip = {router_mac, status}
routers = []  # array of all routers socket
arp_router = {}  # router mac -> router socket


# update the status of a client
def update_client_status(ip, status, router_mac):
    for client, address in client_table.items():  # search for every item
        if client == ip:  # client already exist
            address["status"] = status
            print(f"Status update, {client} is {status}")
            return
    client = {"router_mac": router_mac, "status": status}  # new client found
    client_table[ip] = client  # add new client to the table
    print(f"New client, {ip} is {status}")


# read the client_table and return IP and MAC of the target (if exist)
def check_target_status(target_ip):
    for client, address in client_table.items():  # search for every item
        if client == target_ip:
            return address["status"]  # return status
    return OFFLINE  # target not found


# compose and encode a packet given source IP, destination IP and MAC, message.
def compose_packet(source_ip, destination_ip, destination_mac, message):
    IP_header = source_ip + "|" + destination_ip
    ethernet_header = server_mac + "|" + destination_mac
    packet = ethernet_header + "|" + IP_header + "|" + message
    return str.encode(packet)


# decode and construct array dictionary with the information of packet
def decompose_packet(message: bytes):
    tmp = message.decode("utf-8").split("|")
    if len(tmp) == 5:
        packet = {"source_mac": tmp[0],
                  "destination_mac": tmp[1],
                  "source_ip": tmp[2],
                  "destination_ip": tmp[3],
                  "message": tmp[4]}
        return packet
    else:
        return None


# check for new messages
def check_for_message(router):
    r_socket, _, _ = select.select([router], [], [], checkTimeout)  # check for new message
    if not r_socket:  # no message found
        return None
    else:
        messages = []
        for i_socket in r_socket:  # messages found
            packet = i_socket.recv(1024)  # receive the packet
            if packet == b'':  # check if it has content
                continue
            else:
                messages.append(decompose_packet(packet))  # append the message to the list
        if messages:  # list non empty
            return messages  # return the messages
        else:  # no message found
            return None


def check_for_new_connection():
    global arp_router
    r_socket, _, _ = select.select([server], [], [], checkTimeout)  # check for new router
    if not r_socket:  # no connections requests founds
        return None
    else:
        for i_socket in r_socket:  # new router found
            new_router, address = server.accept()
            ack = decompose_packet(new_router.recv(BUF_SIZE))  # get the ACK
            if ack is not None:  # add the new router
                arp_router[ack["source_mac"]] = new_router
                print(f"Router {ack['source_mac']}  is online | {address}")
                routers.append(new_router)


# parse a message and send a response to the proper destination
def parse_packet(packet):
    if packet["message"] == ONLINE or packet["message"] == OFFLINE:  # change of status
        update_client_status(packet["source_ip"], packet["message"], packet["source_mac"])
    else:  # message between client
        print(f"New message from {packet['source_ip']} to {packet['destination_ip']} -> ", end = "")
        status = check_target_status(packet["destination_ip"])  # retrive target status
        if status == OFFLINE:  # target offline, send back to source client
            client_router = arp_router[client_table[packet["source_ip"]]["router_mac"]]  # get client router
            print(f"Target {packet['destination_ip']} is offline, sending back to client {packet['source_ip']}")
            client_router.send(compose_packet(server_ip, packet["source_ip"], packet["source_mac"], f"{packet['destination_ip']} is offline"))
        else:  # forward message
            client_router = arp_router[client_table[packet["destination_ip"]]["router_mac"]]  # get target router
            print(f"Target {packet['destination_ip']} is online, sending the message")
            client_router.send(compose_packet(packet["source_ip"], packet['destination_ip'], packet['destination_mac'], packet["message"]))


print("Running...")
while True:
    check_for_new_connection()  # check for new router
    packets = []
    for router in routers:  # check for messages from every router
        packets.append(check_for_message(router))
    if packets is None:  # no new message
        continue
    else:
        for packet in packets:
            if packet:  # parse the messages
                parse_packet(packet.pop())
            else:
                continue
