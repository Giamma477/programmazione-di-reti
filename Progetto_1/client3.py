import select
import signal
import socket
import sys

client_table = {  # client table to locate IP and MAC address
    "client1": {"ip": "92.10.10.15", "mac": "32:04:0A:EF:19:CF"},
    "client2": {"ip": "92.10.10.20", "mac": "10:AF:CB:EF:19:CF"},
    "client4": {"ip": "1.5.10.15", "mac": "42:A3:1B:DA:12:AC"},
    "client5": {"ip": "1.5.10.20", "mac": "42:A3:5B:DA:13:EF"},
    "client6": {"ip": "1.5.10.30", "mac": "44:BF:5B:DA:11:AC"}
}
my_ip = "92.10.10.25"
my_mac = "AF:04:67:EF:19:DA"
router = ("127.0.0.1", 83)  # router IP and PORT
socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # create array TCP socket

print("Connecting to router...")
socket.connect(router)  # connect to the router
print("Connection established")
checkTimeout = 1  # set timeout value (sec) for select.select
readable = [socket]  # make socket an iterable object (to use in select.select)


# compose and encode a packet given IP, MAC and message
def compose_packet(destination_ip, destination_mac, message):
    IP_header = my_ip + "|" + destination_ip
    ethernet_header = my_mac + "|" + destination_mac
    packet = ethernet_header + "|" + IP_header + "|" + message
    return str.encode(packet)


# decode and construct array dictionary with the information of packet
def decompose_packet(message: bytes):
    tmp = message.decode("utf-8").split("|")
    if len(tmp) == 5:
        packet = {"source_mac": tmp[0],
                  "destination_mac": tmp[1],
                  "source_ip": tmp[2],
                  "destination_ip": tmp[3],
                  "message": tmp[4]}
        return packet
    else:
        return None


# identificate the client with the router
def identification():
    socket.send(compose_packet(my_ip, my_mac, "ACK"))  # send an ACK
    message = decompose_packet(socket.recv(1024))  # wait for the response
    if message["message"] == "ACK":  # identified
        print("Identification done.")
    else:  # identification failed
        choice = ""
        while choice != "Y" and choice != "N":
            choice = input("Identification ERROR, retry? (Y/N): ")  # retry?
            if choice == "Y":  # retry
                identification()
                break
            elif choice == "N":  # abort
                print("Exit.")
                socket.close()
                sys.exit()
            else:  # wrong input
                continue


# read the client_table and return IP and MAC of the target (if exist)
def locate_target(target):
    target_ip = ""
    target_mac = ""
    for client, address in client_table.items():  # search for every item
        if client == target:  # client found
            target_ip = address["ip"]
            target_mac = address["mac"]
    return target_ip, target_mac  # both equals "" if not found


# check if there are new messages, retrive and print them
def receive_message():
    print("Waiting for new messages... (Ctrl+C to send array message)")
    while True:
        r_socket, _, _ = select.select(readable, [], [], checkTimeout)  # check for new message
        if not r_socket:  # no message found
            continue
        else:
            for i_socket in r_socket:  # messages found
                packet = i_socket.recv(1024)  # receive the packet
                if packet == b'':  # check if it has content
                    continue
                else:  # has content
                    message = decompose_packet(packet)
                    print(f"New message from {message['source_ip']}: {message['message']}")  # print the message
                    print("Waiting for new messages... (Ctrl+C to send array message)")


# ask for array client and array message and send them via the router
def send_message(signal, frame):  # handler for Ctrl+C signal
    target_ip, target_mac = "", ""
    while not target_ip and not target_mac:  # repeat until target is valid
        target = input("Write the destination (es client2), write # to terminate: ")  # target client input
        if target == "#":  # close the client
            socket.send(compose_packet(my_ip, my_mac, "Offline"))
            socket.close()
            sys.exit()
        message = input("Write the message: ")  # message
        target_ip, target_mac = locate_target(target)  # retrive target information
        if not target_ip and not target_mac:  # invalid target
            print("Client not found, try insert array new client.")
            continue
        else:  # send message
            print(f"Message send to {target} | IP: {target_ip} | MAC:{target_mac}")
            socket.send(compose_packet(target_ip, target_mac, message))
    receive_message()  # return to listen for new message


print("Identifying...")
identification()  # identifying
signal.signal(signal.SIGINT, send_message)  # setting Ctrl+C as interrupt
receive_message()  # start to listen for new message
