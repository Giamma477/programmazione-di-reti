import http
import signal
import socketserver
import sys
import http.server

port = 80  # setup port

server = socketserver.ThreadingTCPServer(('', port), http.server.SimpleHTTPRequestHandler)  # setup ThreadingTCPServer
server.daemon_threads = True
server.allow_reuse_address = True


# Signal handler to shutdown the server
def signal_handler(signal, frame):
    print('Exiting http server (Ctrl+C pressed)')
    try:
        if server:
            server.server_close()  # close the server
    finally:
        sys.exit(0)


# set up signal break
signal.signal(signal.SIGINT, signal_handler)
try:
    while True:
        server.serve_forever()  # run the server
except KeyboardInterrupt:
    pass

server.server_close()
